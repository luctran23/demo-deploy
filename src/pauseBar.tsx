import styled from "styled-components";
import { FooterSection, Sidebar, SidebarBodySection, SidebarBodySectionTop } from "./sidebar";

const ButtonTextWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background: red;
`;

const ItemListWrapper = styled.div`
display: grid;
grid-template-columns: 150px 150px;
gap: 16px;
margin: 16px 0px;
`;

const ItemWrapper = styled.div`
display: flex;
-webkit-box-pack: justify;
justify-content: space-between;
padding: 8px 10px;
-webkit-box-align: center;
align-items: center;
gap: 8px;
flex: 1 0 0px;
border-radius: 4px;
cursor: pointer;
border: 1px solid transparent;
background: rgb(245, 245, 245);
`;

export const PauseBar = ({ isOpen }: { isOpen: boolean; }) => {
    return (
        <Sidebar isOpen={isOpen} headerTitle="Pause Services" onClose={close}>
            <SidebarBodySection>
                <SidebarBodySectionTop>
                    <div>
                        Set services you want to pause and how many minutes
                    </div>
                    <div>
                        Service:
                    </div>
                    <ItemListWrapper>
                        <ItemWrapper>item content</ItemWrapper>
                        <ItemWrapper>item content</ItemWrapper>
                        <ItemWrapper>item content</ItemWrapper>
                    </ItemListWrapper>
                    <div>------------</div>
                    <div>
                        Duration:
                    </div>
                    <ItemListWrapper>
                        <ItemWrapper>item content</ItemWrapper>
                        <ItemWrapper>item content</ItemWrapper>
                        <ItemWrapper>item content</ItemWrapper>
                    </ItemListWrapper>
                    <input height={50}/>
                    <div>------------</div>
                    <div>
                        Reason:
                    </div>
                    <ItemListWrapper>
                        <ItemWrapper>item content</ItemWrapper>
                        <ItemWrapper>item content</ItemWrapper>
                        <ItemWrapper>item content</ItemWrapper>
                    </ItemListWrapper>                    <input />
                </SidebarBodySectionTop>
                <FooterSection>
                    <button>
                        <ButtonTextWrapper>
                            Confirm
                        </ButtonTextWrapper>
                    </button>
                </FooterSection>
            </SidebarBodySection>
        </Sidebar>
    );
}