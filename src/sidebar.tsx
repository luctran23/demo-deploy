import React, { useEffect } from 'react';
import styled, { css } from 'styled-components';

const SidebarOverlay = styled.div<{ isSidebarOpen: boolean }>`
  position: fixed;
  display: ${({ isSidebarOpen }) => (isSidebarOpen ? 'block' : 'none')};
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 10;
`;

const SidebarWrapper = styled.div<{ isOpen: boolean }>`
  position: absolute;
  height: 100vh;
  background: #fff;
  z-index: 11;
  top: 0;
  right: 0;
  transition: 0.25s;
  ${({ isOpen }) =>
    isOpen
      ? css`
          width: 348px;
        `
      : css`
          width: 0;
          *,
          div {
            display: none;
          }
        `}
`;

const SideBarHeader = styled.div`
  padding: 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  svg {
    cursor: pointer;
  }
`;

const SidebarBody = styled.div`
  height: calc(100% - 56px);
  overflow: auto;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

const SidebarBodyTop = styled.div`
  padding: 16px;
`;

const FooterWrapper = styled.div`
  display: flex;
  padding: 12px 24px 24px 24px;
  align-items: center;
  gap: 8px;
  align-self: stretch;
  border-top: 1px solid #dfdfdf;
  background: #fefefe;
  width: 100%;
`;

type SidebarProps = {
  isOpen: boolean;
  children: React.ReactNode;
  headerTitle: string;
  onClose: () => void;
};

export const Sidebar: React.FC<SidebarProps> = ({ isOpen, headerTitle, onClose, children }) => {
  useEffect(() => {
    const scrollRootElement = document.getElementById('scroll-root');
    if (scrollRootElement && isOpen) {
      scrollRootElement.style.overflowY = 'hidden';
    }
    if (scrollRootElement && !isOpen) {
      scrollRootElement.style.overflowY = 'scroll';
    }
  }, [isOpen]);
  return (
    <>
      <SidebarOverlay isSidebarOpen={isOpen} />
      <SidebarWrapper isOpen={isOpen}>
        <SideBarHeader>
          <div>
            {headerTitle}
          </div>
          <div onClick={onClose}>x</div>
        </SideBarHeader>
        {children}
      </SidebarWrapper>
    </>
  );
};

export const SidebarBodySection = SidebarBody;
export const SidebarBodySectionTop = SidebarBodyTop;
export const FooterSection = FooterWrapper;
